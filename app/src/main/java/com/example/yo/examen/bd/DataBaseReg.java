package com.example.yo.examen.bd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseReg extends SQLiteOpenHelper {


    public static final int VERSION_DB = 1;
    public static final String NAME_DB = "Prueba.db";

    /***** Tabla usuarios  *****/
    public static final String TABLA_USUARIOS = "usuarios";
    public static final String COLUMNA_ID = "id";
    public static final String COLUMNA_NOMBRE = "nombre";


    private static final String CREAR_TABLA = "create table "
            + TABLA_USUARIOS + "(" + COLUMNA_ID
            + " integer primary key autoincrement, " + COLUMNA_NOMBRE
            + " text not null);";


    public DataBaseReg (Context context) {   //constructor
        super(context, NAME_DB, null, VERSION_DB);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREAR_TABLA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public int insertar(String nombre){
        long nId;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();  //almacenar temporalmente
        cv.put(COLUMNA_NOMBRE, nombre);
        nId = db.insert(TABLA_USUARIOS, null,cv);
        db.close();

        return (int) nId;
    }


    public String obtener(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        String[] col = {COLUMNA_ID, COLUMNA_NOMBRE};

        Cursor cursor = db.query(TABLA_USUARIOS,
                col,"id = ?",
                new String[] { String.valueOf(id) },
                null,
                null,
                null,
                null);

        if (cursor != null)
            cursor.moveToFirst();
        String n = cursor.getString(1);

        //System.out.println("Hi " +  cursor.getString(1) );
        db.close();
        return n;
    }


    public int datosTabla() {
        String contarQ = "SELECT  * FROM " + TABLA_USUARIOS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(contarQ, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }


}
