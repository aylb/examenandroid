package com.example.yo.examen.tabs;

import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yo.examen.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.net.MalformedURLException;
import java.net.URL;


import javax.net.ssl.HttpsURLConnection;

import com.example.yo.examen.bd.DataBaseReg;

public class Tab_Productos extends Activity {

    String nPersona = "";

    DataBaseReg db;
    TextView tv_fjson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.informacion_producto);
        tv_fjson  = (TextView) findViewById(R.id.tv_fjson);

        db = new DataBaseReg( getApplicationContext() );
        int id= 1;

        Bundle saludo = this.getIntent().getExtras();

        nPersona= saludo.getString("SALUDO") + db.obtener(id);

        Toast toast1 = Toast.makeText(getApplicationContext(), nPersona, Toast.LENGTH_SHORT);

        getData();
        toast1.show();

    }



    public void getData(){
        String sql = "https://super.walmart.com.mx/api/rest/model/atg/commerce/catalog/ProductCatalogActor/getSkuSummaryDetails?storeId=0000009999&upc=00750129560012&skuId=00750129560012";

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        URL url = null;
        HttpsURLConnection conn;

        try {
            url = new URL(sql);
            conn = (HttpsURLConnection) url.openConnection();
            conn.setRequestMethod("GET"); //conn.connect();


            conn.setRequestProperty("Accept", "application/json; charset=utf-8");
            conn.setRequestProperty("Connection", "keep-alive");
            conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestProperty("Cookie", "JSESSIONID_GR=ewnljk%2BfgPUYt2Uks5PY-vG9.restapp-298183240-6-359372925; TS01f4281b=0130aff232b466ffcc4072d1bbce44ecbbd89f5d479ff3da4fb92ddaa94160888e2ca908d1fa72efd98d848e3e8531b6c0d47a99fe; akavpau_vp_super=1544643414~id%3D0b950c261050bdafe78b05c390a1fd75; dtCookie=%7CbWV4aWNvX19ncm9jZXJpZXN8MA; TS01c7b722=0130aff232b466ffcc4072d1bbce44ecbbd89f5d479ff3da4fb92ddaa94160888e2ca908d1fa72efd98d848e3e8531b6c0d47a99fe");

            conn.connect();

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            String inputLine;

            StringBuffer response = new StringBuffer();

            String json = "";

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            json = response.toString();
            String mensaje = "";

            JSONObject jsonObject = new JSONObject(json);
            String skuDisplayNameText = jsonObject.getString("skuDisplayNameText");
            System.out.println("valor skuDisplayNameText: " + skuDisplayNameText);

            mensaje +=  skuDisplayNameText;

            tv_fjson.setText(mensaje);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
