package com.example.yo.examen;

import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import wmt.bd.DataBaseReg;
import wmtp.tabs.Tab_Productos;
import wmtp.tabs.Tab_Registro_Nombre;

public class MainActivity extends AppCompatActivity {

    String msgBvnd = "";
    int ndatos = 0;

    DataBaseReg db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        db = new DataBaseReg( getApplicationContext() );
        msgBvnd = "Bienvenido nuevamente ";

        ndatos = db.datosTabla();
        db.close();


        if (ndatos == 0){
            Intent intent = new Intent(MainActivity.this, Tab_Registro_Nombre.class);
            startActivity(intent);

        }else if (ndatos > 0){
            Intent intent = new Intent(MainActivity.this, Tab_Productos.class);
            Bundle b = new Bundle();
            b.putString("SALUDO", msgBvnd);
            intent.putExtras(b);
            startActivity(intent);
        }


    }
}
