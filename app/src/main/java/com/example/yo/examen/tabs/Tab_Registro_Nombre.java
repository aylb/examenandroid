package com.example.yo.examen.tabs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.yo.examen.R;

import com.example.yo.examen.bd.DataBaseReg;

public class Tab_Registro_Nombre extends AppCompatActivity {

    String msgSaludo = "";

    EditText etNombre;
    Button btnComenzar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registro);

        msgSaludo = "Hola ";
        etNombre = (EditText)findViewById(R.id.et_nombre);
        btnComenzar = (Button)findViewById(R.id.btn_comenzar);


        btnComenzar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DataBaseReg db = new DataBaseReg( getApplicationContext() );
                int id = db.insertar(etNombre.getText().toString());
                System.out.println(id);

                Intent intent = new Intent(Tab_Registro_Nombre.this, Tab_Productos.class);
                Bundle b = new Bundle();
                b.putString("SALUDO", msgSaludo);
                intent.putExtras(b);
                startActivity(intent);

            }
        });
    }


}
